#!/bin/bash

for i in "$@"; do
  case $i in
    --debug) debug=true;;
    --clean) clean=true;;
    --prebuilt) prebuilt=true;;
    --full) full=true;;
  esac
done

if [ "$clean" = true ]; then
  rm -rf ./libs
  rm -rf ./obj
  rm -rf ./bin
  rm -rf ./gen
  exit
fi

flags="-j $(nproc)"
if [ "$debug" = true ]; then
  flags="${flags} DEBUG=1"
fi
if [ "$prebuilt" = true ]; then
  flags="${flags} PREBUILT=1"
fi

if [ "$full" = true ]; then
  copyflags="--full"
  suffix="_full"
fi

(./update_assets.sh ${copyflags})
(./update_strings.sh)

ndk-build ${flags}
ant debug

archs=(
  "armeabi-v7a"
  "arm64-v8a"
  "x86"
  "x86_64"
)

if [ ! "$debug" = true ]; then
  cp bin/com.rvgl.rvgl-debug.apk ../distrib/rvgl_android${suffix}.apk

  if [ ! "$prebuilt" = true ]; then
    for name in ${archs[@]}; do
      cp libs/${name}/libmain.so ../distrib/bin/android/${name}/libmain.so
    done
  fi
fi
